FROM php:7.1-fpm-alpine

RUN docker-php-ext-install mysqli

CMD ["sh","-c","if [ -f /uploads.zip ]; then unzip -o /uploads.zip -d /var/www/html/wp-content; fi && php-fpm"]
