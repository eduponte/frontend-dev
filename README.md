<!--ts-->
- [Docker](#docker)
  * [Prerequisitos](#prerequisitos)
    + [Linux](#linux)
    + [OS/X](#osx)
    + [Windows](#windows)
  * [Clonar frontend-dev repository](#clonar-frontend-dev-repository)
  * [Crear dockers](#crear-dockers)
  * [Restaurar Base de Datos](#restaurar-base-de-datos)
  * [Listo!](#listo)
- [Opciones alternativas a docker](#opciones-alternativas-a-docker)
  * [Linux: LAMP](#linux-lamp)
    + [Instalar prerequisitos](#instalar-prerequisitos)
    + [Clone frontend-dev repository](#clone-frontend-dev-repository)
    + [Clone frontend](#clone-frontend)
    + [Crear e importar database](#crear-e-importar-database)
    + [Configurar Apache](#configurar-apache)
    + [Habilitar el idioma por defecto](#habilitar-el-idioma-por-defecto)
    + [Listo!](#listo-1)
  * [OS/X: MAMP](#osx-mamp)
  * [Windows: WAMP](#windows-wamp)
    + [Instalar Sublime Text editor](#instalar-sublime-text-editor)
    + [Instalar Git for Windows](#instalar-git-for-windows)
    + [Clone frontend-dev repository](#clone-frontend-dev-repository-1)
    + [Instalar WAMP server](#instalar-wamp-server)
    + [Start WAMP server.](#start-wamp-server)
    + [Clone frontend repository](#clone-frontend-repository)
    + [Crear virtual server](#crear-virtual-server)
    + [Habilitar proxy module](#habilitar-proxy-module)
    + [Configurar virtual server](#configurar-virtual-server)
    + [Importar base de datos de desarrollo](#importar-base-de-datos-de-desarrollo)
    + [Habilitar el idioma por defecto](#habilitar-el-idioma-por-defecto-1)
    + [Listo!](#listo-2)
- [Troubleshooting](#troubleshooting)

<!--te-->

Instrucciones para la instalación del entorno de desarrollo del frontend de la web de Pam a Pam. Tanto para Linux como para Windows tienes 2 opciones, o bien con Docker (la opción más sencilla) o bien instalando los recursos necesarios en nuestro pc.

# Docker

## Prerequisitos

### Linux

Instalar Git LFS

```bash
sudo apt-get install git git-lfs
```

Instalar docker

```bash
wget -qO- https://get.docker.com/ | sh
sudo usermod -aG docker $USER
```

Para activar el nuevo grupo **docker** agregado al usuario es necesario hacer logout y login en el linux antes de continuar. En algunas distribuciones de linux (Ubuntu 18.04 por ejemplo) hay que esperar unos 10 o 20 segundos entre el logout y el login para que se cierren todas las sesiones abiertas.

Instalar docker-compose

```bash
# sustituir 1.23.2 por la última versión
sudo curl -L "https://github.com/docker/compose/releases/download/1.23.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
```

### OS/X

Instalar `homebrew` y Git LFS

```bash
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
brew install git git-lfs
git lfs install
```

Instalar docker

```bash
brew cask install docker
open /Applications/Docker.app
```
Esperar hasta que el **Docker Desktop** quede **up and running!**

### Windows

_TO DO_

## Clonar frontend-dev repository

```bash
git lfs clone git@gitlab.com:pamapam/frontend-dev.git
cd frontend-dev
git clone git@gitlab.com:pamapam/frontend.git # o fork del usuario sobre el que se va a desarrollar https://gitlab.com/xinxeta43/frontend.git
```

## Crear dockers

```bash
docker-compose up -d
```
## Restaurar Base de Datos

```bash
docker exec -i pamapam-web-db sh -c 'exec mysql -u root -p"$MYSQL_ROOT_PASSWORD" --default-character-set=utf8 ' < ./resources/pamapam_wp_development.sql
```

## Listo!

```bash
$ docker ps
CONTAINER ID        IMAGE                 COMMAND                  CREATED             STATUS              PORTS                                      NAMES
231e20c179e4        nginx:alpine          "nginx -g 'daemon of…"   3 minutes ago       Up 3 minutes        0.0.0.0:80->80/tcp, 0.0.0.0:443->443/tcp   pamapam
0d0bf97ad278        pamapam/pamapam-web   "docker-php-entrypoi…"   3 minutes ago       Up 3 minutes        9000/tcp                                   pamapam-web
bd0fa902cc2f        mariadb:10            "docker-entrypoint.s…"   6 minutes ago       Up 6 minutes        3306/tcp                                   pamapam-web-db
```

Para acceder normalmente al site local de desarrollo:

http://pamapam.local

Admin page de Wordpress:

http://pamapam.local/wp-admin

User: ```_admin_```  
Password: ```pamapam```  

Para detener los contenedores del entorno local:

```bash
docker-compose down
```

Para hacer start de los contenedores del entorno local:

```bash
docker-compose up -d
```

# Opciones alternativas a docker

## Linux: LAMP

### Instalar prerequisitos

```bash
sudo add-apt-repository ppa:ondrej/php
sudo apt-get update
sudo apt-get install git git-lfs apache2 php7.1 mariadb-server php7.1-fpm php7.1-dev php7.1-zip php7.1-curl php7.1-xmlrpc php7.1-gd php7.1-mysql php7.1-mbstring php7.1-xml libapache2-mod-php7.1
```

### Clone frontend-dev repository

```bash
mkdir ~/pamapam
git lfs clone https://gitlab.com/pamapam/frontend-dev.git ~/pamapam/frontend-dev
```

### Clone frontend

```bash
git clone https://gitlab.com/pamapam/frontend.git ~/pamapam/pamapam-web
sudo ln -sv ~/pamapam/pamapam-web /var/www/pamapam-web
cp ~/pamapam/frontend-dev/resources/lamp/wp-config.php ~/pamapam/pamapam-web
unzip ~/pamapam/frontend-dev/resources/uploads.zip -d ~/pamapam/pamapam-web/wp-content
```

### Crear e importar database

```bash
sudo mysql -u root -e "create user pamapam@'%' identified by 'pamapam';"
sudo mysql -u root -e "create database pamapam_wp character set utf8 collate utf8_general_ci;"
sudo mysql -u root -e "grant all on pamapam_wp.* to pamapam@'%' identified by 'pamapam';"
mysql -u pamapam -ppamapam < ~/pamapam/frontend-dev/resources/pamapam_wp_development.sql
```

### Configurar Apache

```bash
sudo cp ~/pamapam/frontend-dev/resources/lamp/pamapam-web.conf /etc/apache2/sites-available/
sudo -- sh -c "echo '127.0.0.1 pamapam.local' >> /etc/hosts"
sudo a2enmod proxy_http
sudo a2ensite pamapam-web
sudo systemctl restart apache2
```

### Habilitar el idioma por defecto

Antes de poder entrar por primera vez a la página de inicio de la web Pam a Pam en lo instalación local, hay que actualizar el idioma por defecto desde el panel de administración del Wordpress:

http://pamapam.local/wp-admin

User: ```_admin_```  
Password: ```pamapam```  

```
Idiomas -> Catalán -> Actualizar
```

### Listo!

Para acceder normalmente al site local de desarrollo:

http://pamapam.local

ó

```
wampserver icon -> left click -> Your VirtualHosts -> pamapam.local
```
## OS/X: MAMP

_TO DO_

## Windows: WAMP

### Instalar Sublime Text editor

https://www.sublimetext.com/

### Instalar Git for Windows

https://gitforwindows.org/

Elegir Sublimetext como editor. Hay un error en el instalador y aún estando instalado el Sublimetext no habilita el botón Next. Hay que hacer Back y Next para poder continuar...

### Clone frontend-dev repository

Open Git Bash

```bash
mkdir ~/pamapam
cd pamapam
git clone https://gitlab.com/pamapam/frontend-dev.git
```

### Instalar WAMP server

Instalar primero todos los Visual C++ Packages. Si es un windows 64 bits hay que instalar también todos los packages 32 bits. Instalarlos en el orden en que aparecen en las listas.

http://wampserver.aviatechno.net/?lang=en#vcpackages

Instalar la última full version (3.1.4 en este momento). Elegir Sublime Text editor como editor por defecto.

http://wampserver.aviatechno.net/

### Start WAMP server.

Esperar a que el ícono se ponga en verde.

### Clone frontend repository

Open Git Bash

```bash
git clone https://gitlab.com/pamapam/frontend.git /c/wamp64/www/pamapam-web
cp ~/pamapam/frontend-dev/resources/wamp/wp-config.php /c/wamp64/www/pamapam-web
unzip ~/pamapam/frontend-dev/resources/uploads.zip -d /c/wamp64/www/pamapam-web/wp-content
```

### Crear virtual server

```
wampserver icon -> left click -> Your VirtualHosts -> VirtualHost Management
```

Name of the Virtual Host: **pamapam.local**  
Complete absolute path of the VirtualHost folder: **c:/wamp64/www/pamapam-web**  

```
wampserver icon -> right click -> Tools -> Restart DNS
wampserver icon -> left click -> PHP -> Version -> 7.1.xx
wampserver icon -> right click -> Tools -> Change PHP CLI version -> 7.1.xx
```

### Habilitar proxy module

Para poder conectarse al backoffice de QA hay que configurar el apache para que haga las redirecciones adecuadas.

```
wampserver icon -> left click -> Apache -> Apache modules -> proxy module
wampserver icon -> left click -> Apache -> Apache modules -> proxy http module
```

### Configurar virtual server

```
wampserver icon -> left click -> Apache -> httpd-vhosts.conf
```

Cambiar:

```apache
<VirtualHost *:80>
	ServerName pamapam.local
	DocumentRoot "c:/wamp64/www/pamapam-web"
	<Directory  "c:/wamp64/www/pamapam-web/">
		Options +Indexes +Includes +FollowSymLinks +MultiViews
		AllowOverride All
		Require local
	</Directory>
</VirtualHost>
```

por:

```apache
<VirtualHost *:80>
	ServerName pamapam.local
	DocumentRoot "c:/wamp64/www/pamapam-web"
	<Directory  "c:/wamp64/www/pamapam-web/">
		Options +Indexes +Includes +FollowSymLinks +MultiViews
		AllowOverride All
		Require local
	</Directory>
	ProxyPass "/backoffice" "http://qa.pamapam.org/backoffice"
	ProxyPassReverse "/backoffice" "http://qa.pamapam.org/backoffice"
	ProxyPass "/backoffice/" "http://qa.pamapam.org/backoffice/"
	ProxyPassReverse "/backoffice/" "http://qa.pamapam.org/backoffice/"
	ProxyPass "/services/" "http://qa.pamapam.org/services/"
	ProxyPassReverse "/services/" "http://qa.pamapam.org/services/"
</VirtualHost>
```

```
wampserver icon -> left click -> Apache -> Service administration -> Restart Service
```

### Importar base de datos de desarrollo

```
wampserver icon -> left click -> phpMyAdmin
```

User: ```root```  
Password: *vacia*  

```
Importar -> Browse
```

Seleccionar: ~/pamapam/frontend-dev/resources/pamapam_wp_development.sql

Iniciar la importación.

### Habilitar el idioma por defecto

Antes de poder entrar por primera vez a la página de inicio de la web Pam a Pam en lo instalación local, hay que actualizar el idioma por defecto desde el panel de administración del Wordpress:

http://pamapam.local/wp-admin

User: ```_admin_```  
Password: ```pamapam```  

```
Idiomas -> Catalán -> Actualizar
```

### Listo!

Para acceder normalmente al site local de desarrollo:

http://pamapam.local

ó

```
wampserver icon -> left click -> Your VirtualHosts -> pamapam.local
```

# Troubleshooting

Solución de problemas conocidos [aquí](./TROUBLESHOOTING.md)
