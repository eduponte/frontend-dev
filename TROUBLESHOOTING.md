### El puerto 80 está ocupado (Ubuntu 18.04)

```
ERROR: for pamapam  Cannot start service pamapam: driver failed programming external connectivity on endpoint pamapam (b1fe3f47a1104f09b630d021a1351b099f3adb3025da390910b7b525cc280ebc): Error starting userland proxy: listen tcp 0.0.0.0:80: bind: address already in use
ERROR: Encountered errors while bringing up the project.
```

```bash
sudo apache2ctl stop
```

### Error establishing a database connection

```
Error establishing a database connection
```

Volver a Restaurar Base de Datos
```bash
docker exec -i pamapam-web-db sh -c 'exec mysql -u root -p"$MYSQL_ROOT_PASSWORD" --default-character-set=utf8 ' < ./resources/pamapam_wp_development.sql
```

### Contenedor se reinicia continuamente

El contenedor está fallando por algún motivo, y como es un servicio que debe vivir siempre se va reiniciando continuamente.
```bash
CONTAINER ID        IMAGE                 COMMAND                  CREATED             STATUS                                  PORTS                                      NAMES
201635d06929        nginx:alpine          "nginx -g 'daemon of…"   9 seconds ago       Up 5 seconds                            0.0.0.0:80->80/tcp, 0.0.0.0:443->443/tcp   pamapam
e229b6672b21        pamapam/pamapam-web   "docker-php-entrypoi…"   10 seconds ago      Restarting (1) Less than a second ago                                              pamapam-web
032014a12a8a        mariadb:10            "docker-entrypoint.s…"   10 seconds ago      Up 9 seconds                            3306/tcp                                   pamapam-web-db
```

Encontrar la causa:
```bash
docker logs --tail 200 -f pamapam-web # or container that keeps restarting over and over
```

Una vez solucionada la causa:
```bash
docker-compose restart pamapam-web # or container that keeps restarting over and over
```
o si está definitivamente parado
```bash
docker-compose up -d pamapam-web # or container that keeps restarting over and over
```
